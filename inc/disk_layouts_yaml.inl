#pragma once
#include "pch.hpp"

#include "conf.inl"

inline void MakeLayouts()
{
	std::ofstream file(ConfDir / "disk_layouts.yml");
	if(!file.is_open()) throw std::exception(); // TODO: Proper Result

	file <<
		"wars_disks:\n"
		"  - name: wars_0\n"
		"    label: main\n"
		"\n"
		"    files:\n"
		"      - name: character\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: Buddy\n"
		"            type: PAC\n"
		"          - name: Sonic\n"
		"            type: PAC\n"
		"          - name: PlayerCommon\n"
		"            type: PAC\n"
		"      - name: customize\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: ctp_body\n"
		"            type: Folder\n"
		"            files:\n"
		"              - name: body_001\n"
		"                type: PAC\n"
		"          - name: ctp_pattern\n"
		"            type: Folder\n"
		"            files:\n"
		"              - name: pattern_002\n"
		"                type: PAC\n"
		"      - name: EffectCommon\n"
		"        type: PAC\n"
		"\n"
		"  - name: wars_1\n"
		"    label: secondary\n"
		"\n"
		"    files:\n"
		"      - name: ui_rpl_texture\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: ui_wm_w1a04\n"
		"            type: PAC\n"
		"\n"
		"  - name: wars_dlc01\n"
		"    label: episode_shadow\n"
		"\n"
		"    files:\n"
		"      - name: dlc01_stgmission\n"
		"        type: .lua\n"
		"\n"
		"  - name: wars_dlc02\n"
		"    label: super_sonic\n"
		"\n"
		"    files:\n"
		"      - name: character\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: SuperSonic\n"
		"            type: PAC\n"
		"\n"
		"  - name: wars_dlc03\n"
		"    label: sega_pack\n"
		"\n"
		"    files:\n"
		"      - name: cutomize\n"
		"        type: Folder\n"
		"        files:\n"
		"            - name: ctp_body\n"
		"              type: Folder\n"
		"              files:\n"
		"                - name: body_049\n"
		"                  type: PAC\n"
		"\n"
		"  - name: wars_dlc04\n"
		"    label: sanic_shirts\n"
		"\n"
		"    files:\n"
		"      - name: cutomize\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: ctp_body\n"
		"            type: Folder\n"
		"            files:\n"
		"              - name: body_053\n"
		"                type: PAC\n"
		"\n"
		"  - name: wars_dlc05\n"
		"    label: shadow_costume\n"
		"\n"
		"    files:\n"
		"      - name: cutomize\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: ctp_glove\n"
		"            type: Folder\n"
		"            files:\n"
		"              - name: glove_037\n"
		"                type: PAC\n"
		"\n"
		"  - name: wars_dlc06\n"
		"    label: persona_costume\n"
		"\n"
		"    files:\n"
		"      - name: cutomize\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: ctp_body\n"
		"            type: Folder\n"
		"            files:\n"
		"              - name: body_052\n"
		"                type: PAC\n"
		"\n"
		"  - name: wars_patch\n"
		"    label: patch\n"
		"\n"
		"    files:\n"
		"      - name: ui\n"
		"        type: Folder\n"
		"        files:\n"
		"          - name: ui_defaultmenu_de\n"
		"            type: PAC\n"
		"      - name: NeedleShader\n"
		"        type: PAC\n";
	file.close();
}
