#pragma once
#include "pch.hpp"

namespace Util
{
	inline void YamlForeach(
		Yaml::Node &root, void* params,
		void (*fun)(Yaml::Node&, void* params)) 
	{
		for(auto i = root.Begin(); i != root.End(); i++)
			fun((*i).second, params);
	}
}
