#pragma once
#include "pch.hpp"

#include "terminal_colors.inl"

// TODO: Cripple the logging system on DEPLOY builds.

#define X(x) x,

#define ENUM_ENTRIES		\
	X(StaticAssetMissing)	\

enum class ErrorCode : u32
{
	Success = 0,
	ENUM_ENTRIES
};

#undef X
#define X(x)				\
		case ErrorCode::x:	\
			return #x;		\

inline ccstr ErrorCodeName(const ErrorCode& v)
{
	switch(v)
	{
		case ErrorCode::Success:
			return "Success (Why are you printing this code???)";

		ENUM_ENTRIES

		default:
			return "UNKNOWN";
	}
}

#undef X

struct Result
{
	ErrorCode error;
	
	std::string file;
	u32 line;
	std::string message;

	inline void Print()
	{
		std::cout << ANSI::Reset <<

			ANSI::BG_Red << ANSI::FG_Black << ANSI::Bold <<
			ErrorCodeName(error) << ANSI::Reset <<

			" in " <<

			ANSI::FG_Red << ANSI::Underline <<
			file << ANSI::Reset <<

			" at " <<

			ANSI::FG_Red << ANSI::Underline <<
			line << ANSI::Reset <<

			": " <<

			ANSI::Bold << ANSI::FG_BrightRed <<
			message << ANSI::Reset <<

			"\n";
	}
};
