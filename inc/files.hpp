#pragma once
#include "pch.hpp"

namespace IOL
{
	namespace FileType
	{
		#define FolderDef(x) \
		const static std::string x = #x;

		FolderDef(Folder)
		FolderDef(PAC)

		#undef FolderDef
	};

	struct File
	{
		std::string type;
		std::unordered_map<std::string, File> files;
		std::vector<std::string> overrides;
		std::vector<std::string> excludes;
	};

	struct Disk
	{
		std::string name;
		std::unordered_map<std::string, File> files;
	};
}
