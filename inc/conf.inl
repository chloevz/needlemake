#pragma once
#include "pch.hpp"

static std::filesystem::path ProjectDir = std::filesystem::current_path();

static std::filesystem::path ConfDir =
	std::string(std::getenv("APPDATA")) + "/chloeware";
static std::filesystem::path ToolsDir =
	std::string(std::getenv("LOCALAPPDATA")) + "/Programs/needlemake";

static std::filesystem::path ProgramDir;

static std::filesystem::path WarsDisksDir;
static std::filesystem::path WarsIntDir;
static std::filesystem::path WarsModsDir;

static std::filesystem::path CpkMaker;
static std::filesystem::path PackCpk;

static std::filesystem::path SFPacZip;
static std::filesystem::path SFPac;

static std::filesystem::path ar0pack;
static std::filesystem::path ar0unpack;
