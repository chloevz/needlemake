#pragma once
#include "pch.hpp"

#include "files.hpp"
#include "result.inl"

namespace Disks
{
    inline std::unordered_map<std::string, IOL::Disk> wars;

    Result Load();
}
