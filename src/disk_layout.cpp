#include "pch.hpp"
#include "disk_layout.hpp"

#include "conf.inl"
#include "files.hpp"
#include "result.inl"
#include "util.hpp"

namespace Disks
{
// TODO: See if these two functions can be the same function just with a different type
void LoadFolder(IOL::File &in_file, Yaml::Node &folder)
{
	Util::YamlForeach(folder["files"], &in_file, [](Yaml::Node& yamlFolder, void* input)
	{
		auto i_file = reinterpret_cast<IOL::File*>(input);

		std::string filename = yamlFolder["name"].As<std::string>();
		i_file->files.insert({filename, IOL::File
		{
			.type = yamlFolder["type"].As<std::string>(),
			.files = std::unordered_map<std::string, IOL::File>(),
			.overrides = std::vector<std::string>(),
			.excludes = std::vector<std::string>()
		}});
		IOL::File &file = i_file->files[filename];
		
		if(IOL::FileType::Folder.compare(file.type) == 0)
		{
			LoadFolder(file, yamlFolder);
		}
	});
}
void LoadDisk(IOL::Disk &in_disk, Yaml::Node &folder)
{
	Util::YamlForeach(folder["files"], &in_disk, [](Yaml::Node& yamlFolder, void* params)
	{
		auto i_disk = reinterpret_cast<IOL::Disk*>(params);
		std::string diskName = yamlFolder["name"].As<std::string>();

		i_disk->files.insert({diskName, IOL::File
		{
			.type = yamlFolder["type"].As<std::string>(),
			.files = std::unordered_map<std::string, IOL::File>(),
			.overrides = std::vector<std::string>(),
			.excludes = std::vector<std::string>()
		}});
		IOL::File &file = i_disk->files[diskName];
		
		if(IOL::FileType::Folder.compare(file.type) == 0)
		{
			LoadFolder(file, yamlFolder);
		}
	});
}

Result Load()
{
	Yaml::Node root;
	try
	{
		Yaml::Parse(root, (ConfDir / "disk_layouts.yml").string().c_str());
	}
	catch (const std::exception &e)
	{
		return Result
		{
			.error = ErrorCode::StaticAssetMissing,
			.file = __FILE__,
			.line = __LINE__ - 8,
			.message = // TODO: Put a link to the file on Github here.
			"disk_layouts.yml file is missing or Needlemake doesn't have "
			"read permissions for it. Please make sure the file exists "
			"and is readable by this program on this user."
		};
	}

	Util::YamlForeach(root["wars_disks"], nullptr, [](Yaml::Node& yamlDisk, void*)
	{
		std::string diskName = yamlDisk["label"].As<std::string>();

		wars.insert({diskName, IOL::Disk
		{
			.name = yamlDisk["name"].As<std::string>(),
			.files = std::unordered_map<std::string, IOL::File>()
		}});
		IOL::Disk &disk = wars[diskName];

		LoadDisk(disk, yamlDisk);
	});
	
	return Result();
}
}
