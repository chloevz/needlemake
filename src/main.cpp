#include "pch.hpp"

#ifndef DEBUG
	#define DEBUG
#endif
#ifndef WINDOWS
	#define WINDOWS
#endif

#include "files.hpp"
#include "help.hpp"
#include "result.inl"
#include "disk_layout.hpp"
#include "conf.inl"
#include "disk_layouts_yaml.inl"

struct Options
{
	bool Help; // H

	bool ExtractDisks; // Ex
	bool RextractDisks; // Rx

	bool PrecleanInt; //Pci

	bool MakeProject; // Mk
	bool CleanMod; // Cm

	bool BuildMod; // Bd
	bool CleanInt; // Ci
};

static Options CmdOptions;



namespace Project
{
	static std::string game;
	static std::string title;
	static std::string description;
	static std::string version; // TODO: Don't use an string for this lol
	static std::string author;
	static std::string authorLink;

	static std::unordered_map<std::string, IOL::Disk> disks;
}

void LoadProjectFolder(
	IOL::File &layout_file, IOL::File &file, Yaml::Node &folder)
{
	for(auto i = folder["files"].Begin(); i != folder["files"].End(); i++)
	{
		Yaml::Node &ifolder = (*i).second;
		std::string filename = ifolder["name"].As<std::string>();

		{
			auto got = layout_file.files.find(filename);
			if(got != layout_file.files.end())
			{
				file.files.insert({filename, IOL::File
				{
					.type = layout_file.files[filename].type,
					.files = std::unordered_map<std::string, IOL::File>(),
					.overrides = std::vector<std::string>(),
					.excludes = std::vector<std::string>()
				}});
			}
			else
			{
				// TODO: Warning code
				continue;
			}
		}
		IOL::File &ifile = layout_file.files[filename];
		IOL::File &ifile_proj = file.files[filename];

		// TODO: Handle other package types like AR0.
		if(IOL::FileType::PAC.compare(ifile.type) == 0)
		{
			if(!ifolder["overrides"].IsNone())
			{
				for(auto l = ifolder["overrides"].Begin(); l != ifolder["overrides"].End(); l++)
				{
					ifile_proj.overrides.emplace_back((*l).second.As<std::string>());
				}
			}
			if(!ifolder["excludes"].IsNone())
			{
				for(auto l = ifolder["excludes"].Begin(); l != ifolder["excludes"].End(); l++)
				{
					ifile_proj.excludes.emplace_back((*l).second.As<std::string>());
				}
			}
		}
		else if(IOL::FileType::Folder.compare(ifile.type) == 0)
		{
			LoadProjectFolder(ifile, ifile_proj, ifolder);
		}
	}
}

void LoadProject()
{
	Yaml::Node root;
	try
	{
		Yaml::Parse(root, (ProjectDir / "needlemake.yml").string().c_str());
	}
	catch (const std::exception &e)
	{
		std::cout << e.what() << "\n";
		return;
	}

	Project::game			= root["game"].			As<std::string>();
	Project::title			= root["title"].		As<std::string>();
	Project::description	= root["description"].	As<std::string>();
	Project::version		= root["version"].		As<std::string>();
	Project::author			= root["author"].		As<std::string>();
	Project::authorLink		= root["authorLink"].	As<std::string>();
	
	for(auto i = root["disks"].Begin(); i != root["disks"].End(); i++)
	{
		Yaml::Node &disk = (*i).second;
		std::string diskname = disk["label"].As<std::string>();
		
		{
			auto got = Disks::wars.find(diskname);
			if(got != Disks::wars.end())
			{
				Project::disks.insert({diskname, IOL::Disk
				{
					.name = Disks::wars[diskname].name,
					.files = std::unordered_map<std::string, IOL::File>()
				}});
			}
			else
			{
				// TODO: Warning code
				continue;
			}
		}
		IOL::Disk &idisk = Disks::wars[diskname];
		IOL::Disk &idisk_proj = Project::disks[diskname];

		for(auto j = disk["files"].Begin(); j != disk["files"].End(); j++)
		{
			Yaml::Node &file = (*j).second;
			std::string filename = file["name"].As<std::string>();
			{
				auto got = idisk.files.find(filename);
				if(got != idisk.files.end())
				{
					idisk_proj.files.insert({filename, IOL::File
					{
						.type = idisk.files[filename].type,
						.files = std::unordered_map<std::string, IOL::File>(),
						.overrides = std::vector<std::string>(),
						.excludes = std::vector<std::string>()
					}});
				}
				else
				{
					// TODO: Warning code
					continue;
				}
			}
			IOL::File &ifile = idisk.files[filename];
			IOL::File &ifile_proj = idisk_proj.files[filename];

			// TODO: Handle other package types like AR0.
			if(IOL::FileType::PAC.compare(ifile.type) == 0)
			{
				if(!file["overrides"].IsNone())
				{
					for(auto l = file["overrides"].Begin(); l != file["overrides"].End(); l++)
					{
						ifile_proj.overrides.emplace_back((*l).second.As<std::string>());
					}
				}
				if(!file["excludes"].IsNone())
				{
					for(auto l = file["excludes"].Begin(); l != file["excludes"].End(); l++)
					{
						ifile_proj.excludes.emplace_back((*l).second.As<std::string>());
					}
				}
			}
			else if(IOL::FileType::Folder.compare(ifile.type) == 0)
			{
				LoadProjectFolder(ifile, ifile_proj, file);
			}
		}
	}
}

inline u32 RunExternal(
	const std::filesystem::path &program,
	const std::vector<std::string> &arguments)
{
	std::stringstream ss;
	ss << "\"" << program;

	for(auto &arg : arguments)
		ss << " \"" << arg << "\"";
	ss << "\"";
	return system(ss.str().c_str());
}

void UnpackDisks()
{
	std::filesystem::current_path(WarsDisksDir);
	
	for(auto &disk : Project::disks)
	{
		std::string cpk = (WarsDisksDir / disk.second.name).string();

		if(std::filesystem::exists(cpk))
		{
			std::cout <<
				"Disk folder " << disk.second.name << " already exists.\n";
			if(!CmdOptions.RextractDisks)
				continue;
			std::cout <<
				"Do you want to delete the folder and unpack the disk again?\nY/N:";

			char reply = ' ';
			while(
				reply != 'y' && reply != 'Y' &&
				reply != 'n' && reply != 'N')
					std::cin >> reply;
			if(reply == 'y' || reply == 'Y')
			{
				std::filesystem::remove_all(cpk);
				RunExternal(PackCpk, {cpk + ".cpk"});
			}
		}
		else
			RunExternal(PackCpk, {cpk + ".cpk"});
	}
	std::filesystem::current_path(ProjectDir);
}

void UnpackFile(
	std::unordered_map<std::string, IOL::File> &files,
	std::filesystem::path &diskDir,
	std::filesystem::path &projDir,
	std::filesystem::path &intDir)
{
	for(auto &file : files)
	{
		if(IOL::FileType::Folder.compare(file.second.type) == 0)
		{
			diskDir /= file.first;
			intDir /= file.first;
			projDir /= file.first;

			UnpackFile(
				file.second.files,
				diskDir,
				projDir,
				intDir
			);

			diskDir = diskDir.parent_path();
			intDir = intDir.parent_path();
			projDir = projDir.parent_path();
		}
		else if (IOL::FileType::PAC.compare(file.second.type) == 0)
		{
			diskDir /= file.first + ".pac";
			intDir /= file.first;
			projDir /= file.first;

			std::filesystem::create_directories(intDir.parent_path());
			std::filesystem::create_directories(projDir.parent_path());

			RunExternal(SFPac, {
				diskDir.string(),
			});
			std::filesystem::rename(
				diskDir.parent_path() / file.first, intDir);
			
			diskDir = diskDir.parent_path();
			intDir = intDir.parent_path();
			projDir = projDir.parent_path();

			diskDir /= file.first;
			intDir /= file.first;
			projDir /= file.first;

			for(auto &overide : file.second.overrides)
			{
				diskDir /= overide;
				intDir /= overide;
				projDir /= overide;
				
				if(!std::filesystem::exists(projDir))
					std::filesystem::copy_file(intDir, projDir);

				diskDir = diskDir.parent_path();
				intDir = intDir.parent_path();
				projDir = projDir.parent_path();
			}
			for(auto &exclude : file.second.excludes)
			{
				diskDir /= exclude;
				intDir /= exclude;
				projDir /= exclude;

				std::filesystem::remove(intDir);

				diskDir = diskDir.parent_path();
				intDir = intDir.parent_path();
				projDir = projDir.parent_path();
			}

			diskDir = diskDir.parent_path();
			intDir = intDir.parent_path();
			projDir = projDir.parent_path();
		}
		else
		{
			diskDir /= file.first + file.second.type;
			intDir /= file.first + file.second.type;
			projDir /= file.first + file.second.type;

			if(!std::filesystem::exists(projDir.parent_path()))
				std::filesystem::create_directories(projDir.parent_path());
			if(!std::filesystem::exists(projDir))
				std::filesystem::copy(diskDir, projDir);
			
			diskDir = diskDir.parent_path();
			intDir = intDir.parent_path();
			projDir = projDir.parent_path();
		}
	}
}

void MakeProject()
{
	std::filesystem::path diskDir = WarsDisksDir;
	std::filesystem::path projDir = ProjectDir;
	std::filesystem::path intDir = WarsIntDir;
	
	for(auto &disk_entry : Project::disks)
	{
		diskDir /= disk_entry.second.name;
		projDir /= disk_entry.second.name;
		intDir /= disk_entry.second.name;

		UnpackFile(
			disk_entry.second.files,
			diskDir,
			projDir,
			intDir);
			
		diskDir = diskDir.parent_path();
		projDir = projDir.parent_path();
		intDir = intDir.parent_path();
	}
}

void PackFile(
	std::unordered_map<std::string, IOL::File> &files,
	std::filesystem::path &diskDir,
	std::filesystem::path &projDir,
	std::filesystem::path &intDir,
	std::filesystem::path &modDir)
{
	for(auto &file : files)
	{
		if(IOL::FileType::Folder.compare(file.second.type) == 0)
		{
			diskDir /= file.first;
			projDir /= file.first;
			intDir /= file.first;
			modDir /= file.first;

			PackFile(
				file.second.files,
				diskDir,
				projDir,
				intDir,
				modDir);

			diskDir = diskDir.parent_path();
			projDir = projDir.parent_path();
			intDir = intDir.parent_path();
			modDir = modDir.parent_path();
		}
		else if(IOL::FileType::PAC.compare(file.second.type) == 0)
		{
			diskDir /= file.first;
			projDir /= file.first;
			intDir /= file.first;
			modDir /= file.first + ".pac";

			std::filesystem::create_directories(modDir.parent_path());

			for(auto &f : std::filesystem::directory_iterator(projDir))
			{
				std::filesystem::remove(intDir / f.path().filename());
				std::filesystem::copy(f.path(), intDir / f.path().filename());
			}

			std::filesystem::current_path(intDir.parent_path());
			RunExternal(SFPac, {
				intDir.string(),
			});
			
			for(auto &f : std::filesystem::directory_iterator(intDir.parent_path()))
			{
				if(!f.is_directory())
					std::filesystem::rename(f.path(),
						modDir.parent_path() / f.path().filename());
				std::cout << (modDir.parent_path() / f.path().filename()).string() << "\n";
			}
			std::filesystem::current_path(ProjectDir);

			diskDir = diskDir.parent_path();
			projDir = projDir.parent_path();
			intDir = intDir.parent_path();
			modDir = modDir.parent_path();
		}
		else
		{
			diskDir /= file.first;
			projDir /= file.first + file.second.type;
			intDir /= file.first;
			modDir /= file.first + file.second.type;

			std::filesystem::copy(projDir, modDir);

			diskDir = diskDir.parent_path();
			projDir = projDir.parent_path();
			intDir = intDir.parent_path();
			modDir = modDir.parent_path();
		}
	}
}

void BuildMod()
{
	std::filesystem::path diskDir = WarsDisksDir;
	std::filesystem::path projDir = ProjectDir;
	std::filesystem::path intDir = WarsIntDir;
	std::filesystem::path modDir = WarsModsDir / Project::title / "disk";
	
	for(auto &disk : Project::disks)
	{
		diskDir /= disk.second.name;
		projDir /= disk.second.name;
		intDir /= disk.second.name;
		modDir /= "wars_patch";

		PackFile(
			disk.second.files,
			diskDir,
			projDir,
			intDir,
			modDir);

		diskDir = diskDir.parent_path();
		projDir = projDir.parent_path();
		intDir = intDir.parent_path();
		modDir = modDir.parent_path();
	}
}

void PrintHelp()
{
	// TODO: Interpret Markdown
	std::cout << HelpSource << "\n";
}

Result program()
{
	if(CmdOptions.Help)
	{
		PrintHelp();
		
		return Result();
	}
#ifdef DEBUG
	std::filesystem::current_path(ProjectDir);
#endif

	try
	{
		Yaml::Node root;
		Yaml::Parse(root, (ConfDir / "chloeconf.yml").string().c_str());

		ProgramDir = root["needlemake"]["program_dir"].As<std::string>();

		WarsDisksDir = root["needlemake"]["wars"]["disks_dir"].As<std::string>();
		WarsIntDir = root["needlemake"]["wars"]["int_dir"].As<std::string>();
		WarsModsDir = root["needlemake"]["wars"]["mods_dir"].As<std::string>();

		CpkMaker	= ToolsDir / "CpkMaker.dll";
		PackCpk		= ToolsDir / "PackCpk.exe";
		SFPacZip	= ToolsDir / "SFPacZip.zip";
		SFPac		= ToolsDir / "SFPac.exe";
		ar0pack		= ToolsDir / "ar0pack.exe";
		ar0unpack	= ToolsDir / "ar0unpack.exe";
	}
	catch (const std::exception &e)
	{
		// TODO: Create an empty config file and
		//       ask the user to fill it out.
		std::cout << "mini-yaml::Exception[" << __LINE__ <<
			"] in " << __FILENAME__ << ": \n" << e.what() << "\n";
		return Result();
	}

	#define DOWNLOAD_STD(path, url)								\
	if(!std::filesystem::exists(path))							\
	{															\
		std::filesystem::create_directories(ToolsDir);			\
		RunExternal("curl", { "-L", "-o", path.string(), url });\
	}															\
	
	DOWNLOAD_STD(CpkMaker,
		"https://raw.githubusercontent.com/blueskythlikesclouds/SkythTools/master/Common/CpkMaker.dll")
	DOWNLOAD_STD(PackCpk,
		"https://raw.githubusercontent.com/blueskythlikesclouds/SkythTools/master/Common/PackCpk.dll")
	// TODO: Uncomment this when Gens support is added
	//DOWNLOAD_STD(ar0pack,
	//	"https://raw.githubusercontent.com/blueskythlikesclouds/SkythTools/master/Sonic+Generations/ar0pack.exe")
	//DOWNLOAD_STD(ar0unpack,
	//	"https://raw.githubusercontent.com/blueskythlikesclouds/SkythTools/master/Sonic+Generations/ar0unpack.exe")
		
	#undef DOWNLOAD_STD

	if(!std::filesystem::exists(SFPac))
	{
		std::filesystem::create_directories(ToolsDir);
		RunExternal("curl", {
			"-L",
			"-o", SFPacZip.string(),
			"https://docs.google.com/uc?export=download&id=1kunzqJIsz5Tzr5zmHb-AaC2Bj516nJoT",
			});

	#ifdef WINDOWS
		std::filesystem::current_path(ToolsDir);
		RunExternal("tar", {
			"-xf", SFPacZip.string()
		});
		std::filesystem::current_path(ProjectDir);
		std::filesystem::remove(SFPacZip);
	#endif
	}
	
	if(!std::filesystem::exists(ConfDir / "disk_layouts.yml"))
		MakeLayouts();
	Disks::Load();
	LoadProject();

	if(CmdOptions.PrecleanInt)
		std::filesystem::remove_all(WarsIntDir);
	
	if(CmdOptions.ExtractDisks)
		UnpackDisks();

	if(CmdOptions.MakeProject)
		MakeProject();

	if(CmdOptions.CleanMod)
		std::filesystem::remove_all(WarsModsDir / Project::title / "disk");

	if(CmdOptions.BuildMod)
		BuildMod();

	if(CmdOptions.CleanInt)
		std::filesystem::remove_all(WarsIntDir);

	return Result();
}

i32 main(const i32 argc, ccstr *argv)
{
	// CSDR: Should I change this from -OPTIONS to -OPTION0 -OPTION1 -OPTION2???
	if(argc < 2)
	{
		std::cout << "No arguments given, every flag other than H will be assumed.\n";

		CmdOptions.Help				= false;
		CmdOptions.ExtractDisks		= true;
		CmdOptions.RextractDisks	= true;
		CmdOptions.PrecleanInt		= true;
		CmdOptions.MakeProject		= true;
		CmdOptions.CleanMod			= true;
		CmdOptions.BuildMod			= true;
		CmdOptions.CleanInt			= true;
	}
	else
	{
		if(std::string(argv[1]).find("H"  ) != std::string::npos) CmdOptions.Help			= true;
		if(std::string(argv[1]).find("Ex" ) != std::string::npos) CmdOptions.ExtractDisks	= true;
		if(std::string(argv[1]).find("Rx" ) != std::string::npos) CmdOptions.RextractDisks	= true;
		if(std::string(argv[1]).find("Pci") != std::string::npos) CmdOptions.PrecleanInt	= true;
		if(std::string(argv[1]).find("Mk" ) != std::string::npos) CmdOptions.MakeProject	= true;
		if(std::string(argv[1]).find("Cm" ) != std::string::npos) CmdOptions.CleanMod		= true;
		if(std::string(argv[1]).find("Bd" ) != std::string::npos) CmdOptions.BuildMod		= true;
		if(std::string(argv[1]).find("Ci" ) != std::string::npos) CmdOptions.CleanInt		= true;
	}

	Result result = program();
}
